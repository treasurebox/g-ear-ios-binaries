#!/bin/bash

cd $(dirname "$0")
BASE=$PWD

usage() {
	cat <<END

usage: $0 -i 'signing identity' -p 'provisioning profile path'
	-l 'external library path' -k 'keychain path'
	-t 'team id' -b 'bundle id'
END
}

die() {
	echo "$*"
	exit 1
}

check_codesigning_id() {	security -q find-identity -vp codesigning "$2" | grep "$1"
	return $?
}

make_xcent() {
	cat <<END>misc/G-Ear\ iOS.xcent
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>application-identifier</key>
	<string>$1.$2</string>
	<key>get-task-allow</key>
	<true/>
	<key>keychain-access-groups</key>
	<array>
		<string>$1.$2</string>
	</array>
</dict>
</plist>
END
}

fill_plist() {
	printf "$(<app/G-Ear\ iOS.app/Info.plist.temp)\n" $1>app/G-Ear\ iOS.app/Info.plist
}

format_security_output() {
	local x=${1#*\"}
	echo "${x%\"*}"
}

compile_gear() {
	local dev_base=$(xcode-select -print-path)
	$dev_base/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang++ \
	-arch armv7 \
	-isysroot $dev_base/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS7.1.sdk \
	-L"$EXT_LIB_PATH" \
	-Llib \
	-filelist obj/link_filelist \
	-dead_strip \
	-lcore \
	-lmpg123 \
	-lavformat \
	-lavcodec \
	-lavresample \
	-lavutil \
	-stdlib=libc++ \
	-fobjc-arc \
	-fobjc-link-runtime \
	-miphoneos-version-min=6.1 \
	-framework SystemConfiguration \
	-framework AVFoundation \
	-framework MediaPlayer \
	-framework QuartzCore \
	-framework Security \
	-framework AudioToolbox \
	-framework CoreData \
	-framework UIKit \
	-framework Foundation \
	-framework CoreGraphics \
	-framework StoreKit \
	-o app/G-Ear\ iOS.app/G-Ear\ iOS
}

sign_gear() {
	codesign \
	--keychain "$KEYCHAIN" \
	--force \
	--sign "$SIGN_ID" \
	--resource-rules=app/G-Ear\ iOS.app/ResourceRules.plist \
	--entitlements misc/G-Ear\ iOS.xcent \
	app/G-Ear\ iOS.app
}

pack_gear() {
	xcrun -sdk iphoneos PackageApplication \
	app/G-Ear\ iOS.app \
	-o "$BASE"/G-Ear.ipa \
	--sign "$SIGN_ID" \
	--embed "$PROVISIONING_PROFILE"
}

export IPHONEOS_DEPLOYMENT_TARGET=6.1
export PATH=/Applications/Xcode.app/Contents/Developer/usr/bin:/usr/bin:/bin:/usr/sbin:/sbin
export CODESIGN_ALLOCATE=/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/codesign_allocate

KEYCHAIN=$(format_security_output $(security default-keychain))
EXT_LIB_PATH=lib/
TEAM_ID=
BUNDLE_ID=
while getopts ":i:p:l:k:t:b:" opt ; do
	case $opt in
		i)
			SIGN_ID=$OPTARG
			;;
		p)
			PROVISIONING_PROFILE=$OPTARG
			;;
		l)
			EXT_LIB_PATH=$OPTARG
			;;
		k)
			KEYCHAIN=$OPTARG
			;;
		t)
			TEAM_ID=$OPTARG
			;;
		b)
			BUNDLE_ID=$OPTARG
			;;
		*)
			die "$(usage)"
	esac
done

(($#)) || die "$(usage)"

[ "$SIGN_ID" ] || die "No signing id given !"

for i in libavcodec.a libavformat.a libavresample.a libavutil.a libcore.a libmpg123.a ; do
	[ -r "$EXT_LIB_PATH/$i" -o -r "lib/$i" ] || die "$i missing !"
done

((${#TEAM_ID} < 10)) && die "Team ID !"
[ "$BUNDLE_ID" ] || die "Bundle ID !"

[ -r "$KEYCHAIN" ] || die "Keychain unreadable !"
[ -r "$PROVISIONING_PROFILE" ] || die "Provisioning profile unreadable !"

check_codesigning_id "$SIGN_ID" "$KEYCHAIN" || die "Signing identity not found on keychain !"
make_xcent $TEAM_ID $BUNDLE_ID
fill_plist $BUNDLE_ID
{ compile_gear && sign_gear && pack_gear;} &>log
